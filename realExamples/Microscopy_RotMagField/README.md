# Transferring Data Files to Coscine

## Coscine Setup

The script requires a Coscine RDS-S3 resource. See [here](https://docs.coscine.de/de/rdm/resources/about/#ressourcentypen) for more information on resource types in Coscine. When setting up the resource in Coscine, pick the Microscopy_RotMagField application profile, located under SFB 985 in the drop-down menu. Here, you can fill-in default values. These can be modified later, as can the name and description of the resource. Just ensure you modify the resource information in the conjig.json file, as well.

Head to your [Coscine profile](https://coscine.rwth-aachen.de/user/) to generate an access token. Save this in token in the config.json file (see [below](#configuration)). These tokens do exprire, so ensure that you keep yours up to date.

*Note: you may also you the web-interface to add files and add/modify metadata to Coscine.*

## Python Script Setup

Ensure Python 3 and the following packages are installed on your system:

- coscine
- os
- datetime
- sys
- logging
- json
- tkinter
- regex
- pathlib

Many of these are standard and will already be installed. Use the [pip installer](https://pip.pypa.io/en/stable/) to easily install any packages through the command line (see [PyPi](https://pypi.org/). Pip should be included when Python is installed, or you can [install it manually](https://pip.pypa.io/en/stable/installation/).

## Configuration

A config.json file should include the following information(replace the information in caps):

```json
{
    "token_cos" : "COSCINE_TOKEN",
    "data_folder" : "DATA_PATH",
    "cos_resource" : "COSCINE_RESOURCE_NAME",
    "cos_project"  : "CCOSCINE_PROJECT_NAME",
    "log_file_name" : "CoscineScriptError.log"
}
```

The project name must match the display name in Coscine. A template can be found [here](config.json). Ensure this config.json file is saved in the same directory as the Python script (or change the path to the file in the script).




